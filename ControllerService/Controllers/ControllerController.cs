﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ControllerService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ControllerController : Controller
	{
		[HttpGet("{name}")]
		public ActionResult<string> Get(string name)
		{
			return "Hello from The Proposal Factory: " + name;
		}
	}
}
