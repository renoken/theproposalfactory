﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ControllerService.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Spa()
		{
			return Content("<!DOCTYPE html>\n<html>\n    <head>\n        <meta charset=\"utf-8\" />\n        <title></title>\n\t\t<link rel=\"stylesheet\" href=\"/static/default.css\" />\n    </head>\n    <body>\n\t\t<div class=\"test\">Hello from The Proposal Factory!</div>\n    </body>\n</html>", "text/html");
		}
	}
}
